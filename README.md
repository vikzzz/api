
## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Install Laravel

1) Install Composer
2) Clone or take the pull of API. Run composer install and update.
3) I have alread added the migrations and seeding.
4) To run the migrattion please run the command from terminal:  php artisan migrate
5) For seeding please run: php artisan db::seed from terminal
6) Now run command php artisan server
7) Laravel will start run on 8000 port
