<?php
    namespace App;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Support\Facades\DB;

    class Flavour extends Model {

        protected $table = 'flavours';

        public static function createFlavour( $name, $description, $size, $quantity, $price ){
            try{
                DB::beginTransaction();
                $flavourId = DB::table('flavours')->insertGetId([
                    'name' => $name,
                    'description' => $description,
                    'status' => 1
                ]);

                $count = count($size);
                $attributesData = [];
                for( $i = 0; $i < $count; $i++ ){
                    $sizeId = 1;
                    if( $size[$i] == "Small" ){
                        $sizeId = 1;
                    }else if( $size[$i] == "Medium" ){
                        $sizeId = 2;
                    }else{
                        $sizeId = 3;
                    }
                    $attributesData[$i]['flavourId'] = $flavourId;
                    $attributesData[$i]['sizeId'] = $sizeId;
                    $attributesData[$i]['quantity'] = $quantity[$i];
                    $attributesData[$i]['price'] = $price[$i];
                }
                $flavourAttributesId = DB::table('flavourattributes')->insert($attributesData);
                DB::commit();
                return true;
            }catch( Exception $e ){
                DB::rollback();
                return false;
            }
        }

        public static function getFlavourById( $id ){
            try {
                $query = DB::table('flavours as a')
                    ->join('flavourattributes as b', 'a.id', '=', 'b.flavourId')
                    ->join('flavoursizes as c', 'b.sizeId', '=', 'c.id');
                $query = $query->leftJoin('soldflavours as d', function( $join ){
                    $join->on('d.flavourId','=', 'a.id');
                    $join->on('d.flavourAttributeId','=', 'b.id');
                });
                $query = $query->select('a.id as flavourId','a.description', 'a.name','b.quantity','b.price','c.name as flavourSize', 'd.quantity as soldQuanity')
                    ->where('a.id','=',$id)
                    ->get();
                return $query;
            }catch( Exception $e ){
                echo $e->getMessage();
            }
        }
    }
