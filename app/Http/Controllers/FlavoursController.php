<?php
    namespace App\Http\Controllers;

    use App\Flavour;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Validator;
    use Illuminate\Http\Request;
    use App\User;

    class FlavoursController extends Controller {
        
        public function create(Request $request) {
            $validator = Validator::make($request->all(), [
                'flavourName' => 'required|between:2,100'
            ]);
            if ( $validator->fails() ){
                return response()->json([
                    'message' =>  $validator->errors(),
                    'success' => 0
                ], 415);
                return $this->apiResponse( $validator->errors(), 0, '' , 415 );
            };
            $name = $request->flavourName;
            $description = $request->description;
            $price = $request->price;
            $quantity = $request->quantity;
            $size = $request->size;


            $createFlavour = Flavour::createFlavour( $name, $description, $size, $quantity, $price );
            if( $createFlavour ){
                return response()->json([
                    'message' => 'Flavour Created Succesfully.',
                    'success' => 1,
                    'data' => $createFlavour
                ], 201);
            }else{
                return response()->json([
                    'message' => $createFlavour,
                    'success' => 0
                ], 500);
            }
        }

        public function getFlavours(){
            $flavours = Flavour::where('status', 1)->orderBy('id','DESC')->get();
            if( $flavours ){
                return $this->apiResponse( 'Flavours List.', 1, $flavours , 200 );
            }else{
                return $this->apiResponse( 'No Flavour stored yet.', 0, $flavours , 200 );
            }
        }

        public function getFlavourById( $id ){
            $flavours = Flavour::getFlavourById($id);
            if( $flavours ){
                $data = [];
                foreach( $flavours as $key => $tempData ){
                    $soldQuantity = $tempData->soldQuanity ?  $tempData->soldQuanity : 0;
                    $data['id'] = $tempData->flavourId;
                    $data['name'] = $tempData->name;
                    $data['description'] = $tempData->description;
                    $data['data'][$key]['quantity'] = $tempData->quantity ? $tempData->quantity - $soldQuantity  : '';
                    $data['data'][$key]['price'] = $tempData->price ? $tempData->price : '';
                    $data['data'][$key]['flavourSize'] = $tempData->flavourSize ? $tempData->flavourSize : '';
                    $data['data'][$key]['soldQuanity'] = $soldQuantity;

                }
                $result = json_decode(json_encode($data),true);
                return $this->apiResponse( 'Flavours List.', 1, $result , 200 );
            }else{
                return $this->apiResponse( 'No Flavour stored yet.', 0, $flavours , 200 );
            }
        }
    }
