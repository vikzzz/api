<?php
    namespace App\Http\Controllers;
    use App\Http\Controllers\Controller;
    use App\FlavourSize;

class SizeController extends Controller {

        public function getSizes(){
            $sizes = FlavourSize::all();
            if( $sizes ){
                return $this->apiResponse( 'Sizes List.', 1, $sizes , 200 );
            }else{
                return $this->apiResponse( 'No Sizes stored yet.', 0, $sizes , 200 );
            }
        }

    }
?>