<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class FlavoursSizesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $flavourSizes = [
            [ 
                'id' => 1,
                'name' => 'Small',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s') ],
            [ 
                'id' => 2,
                'name' => 'Medium',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [ 
                'id' => 3,
                'name' => 'Large',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ];
        DB::table('flavoursizes')->truncate();
        DB::table('flavoursizes')->insert( $flavourSizes);
        $this->command->info('User table seeded!');
    }
}
