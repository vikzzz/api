<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class FlavoursSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('flavours')->truncate();
        DB::table('flavourattributes')->truncate();
        DB::table('soldflavours')->truncate();
        $flavours = [
            [ 
                'id' => 1,
                'name' => 'Flavour 1',
                'status' => 1,
                'description' => "Lorem Ipsum 1",
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s') ],
            [ 
                'id' => 2,
                'name' => 'Flavour 2',
                'status' => 1,
                'description' => "Lorem Ipsum 2",
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [ 
                'id' => 3,
                'name' => 'Flavour 3',
                'description' => "Lorem Ipsum 3",
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [ 
                'id' => 4,
                'name' => 'Flavour 4',
                'description' => "Lorem Ipsum 4",
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [ 
                'id' => 5,
                'name' => 'Flavour 5',
                'description' => "Lorem Ipsum 5",
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [ 
                'id' => 6,
                'name' => 'Flavour 6',
                'description' => "Lorem Ipsum 6",
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [ 
                'id' => 7,
                'name' => 'Flavour 7',
                'description' => "Lorem Ipsum 7",
                'status' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [ 
                'id' => 8,
                'name' => 'Flavour 8',
                'status' => 1,
                'description' => "Lorem Ipsum 8",
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ];

        $flavourAttributes =  [
            [ 
                'id' => 1,
                'flavourId' => 1,
                'sizeId' => 1,
                'quantity' => 10,
                'price' => 100,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [ 
                'id' => 2,
                'flavourId' => 1,
                'sizeId' => 1,
                'quantity' => 100,
                'price' => 100,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [ 
                'id' => 3,
                'flavourId' => 1,
                'sizeId' => 1,
                'quantity' => 100,
                'price' => 100,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [ 
                'id' => 4,
                'flavourId' => 2,
                'sizeId' => 1,
                'quantity' => 3,
                'price' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [ 
                'id' => 5,
                'flavourId' => 3,
                'sizeId' => 2,
                'quantity' => 10,
                'price' => 100,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [ 
                'id' => 6,
                'flavourId' => 4,
                'sizeId' => 1,
                'quantity' => 1,
                'price' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [ 
                'id' => 7,
                'flavourId' => 5,
                'sizeId' => 1,
                'quantity' => 1,
                'price' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [ 
                'id' => 8,
                'flavourId' => 6,
                'sizeId' => 1,
                'quantity' => 1,
                'price' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [ 
                'id' => 9,
                'flavourId' => 7,
                'sizeId' => 1,
                'quantity' => 1,
                'price' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [ 
                'id' => 10,
                'flavourId' => 8,
                'sizeId' => 1,
                'quantity' => 1,
                'price' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
        ];

        $soldflavours = [
            [ 
                'id' => 1,
                'flavourId' => 1 ,
                'flavourAttributeId' => 1,
                'quantity' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [ 
                'id' => 2,
                'flavourId' => 1 ,
                'flavourAttributeId' => 2,
                'quantity' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [ 
                'id' => 3,
                'flavourId' => 1,
                'flavourAttributeId' => 3,
                'quantity' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [ 
                'id' => 4,
                'flavourId' => 2,
                'flavourAttributeId' => 2,
                'quantity' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
        ];

        DB::table('flavours')->insert( $flavours);
        DB::table('soldflavours')->insert( $soldflavours);
        DB::table('flavourattributes')->insert( $flavourAttributes);
    }
}
